import React from 'react';
import './App.scss';
import Gallery from '../Gallery';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.timeout = 0;
    this.state = {
      text: 'art',
      typeOfQuery :'tags'
    };
  }

  delayApiSearch = (event) => {
    let searchText = event.target.value.toLowerCase();
    let fieldName = event.target.name;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(function () {
      this.setState({
        [fieldName]: searchText
      })
    }.bind(this), 500);

  }

  render() {
    
    return (
      <div className="app-root">
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <input name="text" className="app-input" onChange={this.delayApiSearch} />
          <select name="typeOfQuery"  onChange={this.delayApiSearch} className="select-input">
            <option>
              Tags
            </option>
            <option>
              Text
            </option>
          </select>
        </div>
        <Gallery  text={this.state.text} typeOfQuery={this.state.typeOfQuery}/>
      </div>
    );
  }
}

export default App;
