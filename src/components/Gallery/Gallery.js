import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import FontAwesome from 'react-fontawesome'

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      fetchPage:1,

      // for css modal -expend
      modalOpen: false,
      modalClass: 'myModal modalHide',
      imageUrl: ''
    };
  }

   //send to Image -clone
   duplicateImage = (dto) => {
    let arr = this.state.images;
    arr.push(dto);

    this.setState({
      images: arr
    })
  }

  expendImage = (image) => {
    if (!this.state.modalOpen) {
      this.setState({
        modalOpen: true,
        modalClass: 'myModal modalShow',
        imageUrl: image
      })
    }
    else {
      this.setState({
        modalOpen: false,
        modalClass: 'myModal modalHide'
      })
    }
  }

  getGalleryWidth() {
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  getImages(text, typeOfQuery,page) {
    let query = `&page=${page}&${typeOfQuery}=${text}`;
    
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad${query}&tag_mode=any&per_page=40&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'

    })
      .then(res => res.data)
      .then(res => {
        
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          if (page===1) {
            this.setState({ images: res.photos.photo, fetchPage:2});
          }
          else{
            let nextPage = [...this.state.images, ...res.photos.photo];
            this.setState({ images: nextPage, fetchPage:page + 1 });
          }
        }
      });
  }


  handleScroll(){
    if ((Math.ceil(document.documentElement.scrollTop) + document.documentElement.clientHeight) === document.documentElement.scrollHeight) {
      this.getImages(this.props.text,this.props.typeOfQuery,this.state.fetchPage)
    }
  }
    
  componentDidMount() {
    window.addEventListener('scroll', ()=>this.handleScroll());
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }


  componentWillReceiveProps(props) {
    
    this.getImages(props.text,props.typeOfQuery, 1);
  }

  render() {
    return (
      <div className="gallery-root row">
          {this.state.images.map((dto, index) => {
            return <Image
              expendImage={this.expendImage}
              duplicateImage={this.duplicateImage}
              key={'image-' + dto.id + index}
              dto={dto} galleryWidth={this.state.galleryWidth} />;
          })}

        <div className={this.state.modalClass}>
          <img src={this.state.imageUrl} />
            <div className='closeModal'>
                <FontAwesome className="image-icon" onClick={this.expendImage} name="window-close" title="close"/>
            </div>
        </div>
      </div>
    );
  }
}

export default Gallery;
