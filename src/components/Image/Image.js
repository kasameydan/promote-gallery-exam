import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      imageClass: 'image-root',

      //5 CSS filter ClassNames
      filterArr: [
        'contrast',
        'saturate',
        'greyscale',
        'blur',
        'sepia'
      ]
      };
  }

  //Change Image ClassName - filter
  randomizeFilter = () => {
 
    let index = Math.floor(Math.random() * 5);
    this.setState({
      imageClass: 'image-root ' + this.state.filterArr[index]
    })
  }

  expendImage = ()=>{
    this.props.expendImage(this.urlFromDto(this.props.dto))
  }

  duplicateImage = () =>{
    this.props.duplicateImage(this.props.dto)
  }


  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  render() {
    
    return (
      <div
        className={this.state.imageClass}
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px'
        }}
        >
        <div>
          <FontAwesome className="image-icon" onClick={this.expendImage} name="expand" title="expand"/>
          <FontAwesome className="image-icon" onClick={this.randomizeFilter}  name="filter" title="filter"/>
          <FontAwesome className="image-icon" onClick={this.duplicateImage} name="clone"  title="clone"/>
        </div>
      </div>
    );
  }
}

export default Image;
